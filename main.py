import json
from unittest import result
import requests
import sys


api_key = sys.argv[1]
print(type(api_key))


class Weather:

    def __init__(self, api_key):
        self.dates = {}
        self.user_date = ""
        self.raining_codes = ('t04n', 't04d', 't04n', 't04d', 't04n', 't04d', 't03n', 't03d', 't02n', 't02d', 't01n', 't01d')
        self.api_key = api_key


    def menu(self):
        self.get()
        print("Sprawdz date - (1)\nPokaz wszystkie daty - (2)\nPokaz wszystko - (3)")
        option = int(input(': '))
        if option == 1:
            print()
            print("Please select a day to check a weather. From today to 16 days ahead")
            print("Use the format as in the example below")
            print("rrrr-mm-dd")
            print()
            self.user_date = input(': ')

            if self.user_date in self.dates:
                if self.dates[self.user_date] in self.raining_codes:
                    print('Bedzie padac')
                else:
                    print("Nie bedzie padac")
            else:
                print('Nie wiadomo')

        
        elif option == 2:
            for date in self.dates.keys():
                print(date)

        elif option == 3:
            for line in self.item():
                print("{}".format(line))


    def get(self):
        url = "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily"
        querystring = {"lat":"38.5","lon":"-78.5"}
        headers = {
            "X-RapidAPI-Host": "weatherbit-v1-mashape.p.rapidapi.com",
        }
        headers["X-RapidAPI-Key"] = self.api_key
        response = requests.get( url, headers=headers, params=querystring)

        for x in response.json()['data']:
            self.dates[x['valid_date']] = x['weather']['icon']

    
    def item(self):
        for zip in self.dates:
            if self.dates[zip] in self.raining_codes:
                yield zip, 'bedzie padac'
            else:
                yield zip, 'nie bedzie padac'




weather = Weather(api_key)
weather.menu()